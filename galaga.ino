#include <RGBmatrixPanel.h>

#define OE 12
#define LAT 13
#define A A0
#define B A1
#define C A2
#define D A3
#define CLK 8
#define LR 4
#define UD 5
#define B_1 11
#define B_2 10

RGBmatrixPanel matrix(A,B,C,D,CLK,LAT,OE,0);
uint32_t lu=0;
uint8_t *bb=0;
int8_t bx=16;
int8_t by=16;
bool b1=0;
bool b2=0;
int8_t sx=16;
int8_t sy=16;
boolean shot=0;
int8_t e1x=16;
int8_t e1y=30;
boolean lose=0;
uint8_t lflash=0;
int8_t score=0;

void setup()
{
  matrix.begin();
  bb=matrix.backBuffer();
  Serial.begin(9600);
  pinMode(B_1,INPUT);
  pinMode(B_2,INPUT);
}

void loop()
{
    lu=millis();
  uint16_t lr = analogRead(LR);
  uint16_t ud = analogRead(UD);
  bx=max(min(bx+(lr>712)-(lr<312),31),0);
  by=max(min(by-(ud>712)+(ud<312),31),0);
  b1=digitalRead(B_1)==HIGH;
  b2=digitalRead(B_2)==HIGH;
  if(b1&&!shot)
  {
    shot=1;
    sx=bx;
    sy=by;
  }
  sy+=3;
  shot&=sy<32;
  if(e1y<29)
  {
    uint8_t spd=min(score>>1,3)+1;
    if(e1x<5)
    {
      e1y-=2;
      e1x=5;
    }
    else if(e1x>26)
    {
      e1y-=2;
      e1x=26;
    }
    e1x+=spd*((e1y&2)-1);
    lose|=abs(e1y-by)+abs(e1x-bx)<3||e1y==0;
    if(shot&&abs(e1y-sy)+abs(e1x-sx)<3)
    {
      score++;
      e1x=(lu&15)+7;
      e1y=34+(lu&1);
    }
  }
  else e1y--;
  if(lose&&b2)
  {
    bx=16;
    by=16;
    sx=16;
    sy=16;
    shot=0;
    e1x=16;
    e1y=30;
    lose=0;
    score=0;
  }
  lflash=((lu>>6)&B00010000)-1;
  render();
  matrix.swapBuffers(0);
}
void doPixel(uint8_t x, uint8_t y, uint8_t rx, uint8_t *r, uint8_t *g, uint8_t *b)
{
  *r=0;
  *g=0;
  *b=0;
  if(x==0&&y<score)
  {
    *g=7;
    *b=y>>1;
  }
  else if(lose)
  {
    *r=lflash;
  }
  else if(((abs(y-by)*2)+abs(x-bx))<3||(abs(x-bx)<2&&y==by-2))
  {
    *r=15;
    if(abs(x-bx)!=2)
    {
      *g=15;
      *b=15;
    }
  }
  else if(shot&&x==sx&&(abs(y-sy)<2))
  {
    *r=15;
    *g=4;
  }
  else if((abs(y-e1y)+abs(x-e1x))<2)
  {
    *r=13;
    *b=15;
  }
  else if((rx&31)==((y+(lu>>6))&31))
  {
    *r=4;
    *g=4;
    *b=4;
    if ((rx&32)==32)
    {
      *r = 15;
    }
  }
}
void render()
{
  for(uint8_t x=0;x<32;x++)
  {
    uint8_t rx = ((x+132)*(x^73))&0xff;
    for(uint8_t y=0;y<16;y++)
    {
      uint8_t r1, g1, b1, r2, g2, b2;
      doPixel(x,y,rx,&r1,&g1,&b1);
      doPixel(x,y+16,rx,&r2,&g2,&b2);
      uint8_t *ptr1,*ptr2;
      ptr1=ptr2=&bb[y*96+x];
      ptr1[64]&=B11111100;
      *ptr2   &=B11111100;
      if(r1&1)ptr1[64]|=B00000001;
      if(g1&1)ptr1[64]|=B00000010;
      if(b1&1)ptr1[32]|=B00000001;
      else    ptr1[32]&=B11111110;
      if(r2&1)ptr2[32]|=B00000010;
      else    ptr2[32]&=B11111101;
      if(g2&1)*ptr2   |=B00000001;
      if(b2&1)*ptr2   |=B00000010;
      *ptr1&=B11100011;
      *ptr2&=B00011111;
      if(r1&2)*ptr1|=B00000100;
      if(g1&2)*ptr1|=B00001000;
      if(b1&2)*ptr1|=B00010000;
      if(r2&2)*ptr2|=B00100000;
      if(g2&2)*ptr2|=B01000000;
      if(b2&2)*ptr2|=B10000000;
      ptr1+=32;
      ptr2+=32;
      *ptr1&=B11100011;
      *ptr2&=B00011111;
      if(r1&4)*ptr1|=B00000100;
      if(g1&4)*ptr1|=B00001000;
      if(b1&4)*ptr1|=B00010000;
      if(r2&4)*ptr2|=B00100000;
      if(g2&4)*ptr2|=B01000000;
      if(b2&4)*ptr2|=B10000000;
      ptr1+=32;
      ptr2+=32;
      *ptr1&=B11100011;
      *ptr2&=B00011111;
      if(r1&8)*ptr1|=B00000100;
      if(g1&8)*ptr1|=B00001000;
      if(b1&8)*ptr1|=B00010000;
      if(r2&8)*ptr2|=B00100000;
      if(g2&8)*ptr2|=B01000000;
      if(b2&8)*ptr2|=B10000000;
    }
  }
}
